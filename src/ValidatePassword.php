<?php 

class ValidatePassword {
	const MIN_LENGTH = 5;
	const MAX_LENGTH = 20;


	public function ValidLength($password) {
		$passLength = strlen($password);
		return $passLength >= self::MIN_LENGTH &&  $passLength <= self::MAX_LENGTH;
	}
}

?>
